#include "Weapon.h"

Weapon::Weapon(EntityList *entList)
    :Item(vector3(0.0,0.0,0.0),vector3(0.0,0.0,-1.0), entList)
{
    listIndex = glGenLists(1);
}


void Weapon::drawEntity(){
    glCallList(listIndex);
}

void Weapon::movement(){
}

void Weapon::shot_portal(vector3 pos, vector3 dir, vector3* portal_pos, vector3* portal_dir, vector3* portal_up){
    vector3 ld_pos;
    GLfloat min_dist = 10000.0;
    vector3 zeros = vector3(0.0,0.0,0.0);
    list<Entity*>::iterator p;
    for (p = entityList->begin(), p++; p!=entityList->end(); p++){
        if (Wall* wall = dynamic_cast<Wall*>(*p)) {
            ld_pos = wall->shot_detection(pos, dir, &min_dist);
            if(ld_pos!=zeros){
                *portal_pos=ld_pos;
                *portal_dir=wall->getDir();
                *portal_up=wall->getUp();
            }
        }else if (Floor* floor = dynamic_cast<Floor*>(*p)) {
            ld_pos = floor->shot_detection(pos, dir, &min_dist);
            if(ld_pos!=zeros){
                *portal_pos=ld_pos;
                *portal_dir=floor->getDir();
                *portal_up=floor->getUp();
            }
        }else if (Geometry* geometry = dynamic_cast<Geometry*>(*p)) {
            ld_pos = geometry->shot_detection(pos, dir, &min_dist);
            if(ld_pos!=zeros){
                *portal_pos=ld_pos;
                *portal_dir=geometry->getDir();
                *portal_up=geometry->getUp();
            }
        }
    }
}
